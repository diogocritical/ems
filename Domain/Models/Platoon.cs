﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementService.Models
{
    
  public class Platoon
  {
    [Required]
    [Range(0,100)]
    public int ID { get; set; }

    [Required]
    [MaxLength(18)]
    public string Name { get; set; }

    [Required]
    [Range(0,100)]
    public int Members { get; set; }
   
  }
}
