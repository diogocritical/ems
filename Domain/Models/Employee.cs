﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManagementService.Models
{
    public class Employee
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int ID { get; set; }

        [Required]
        [MaxLength(60)]
        public string Name { get; set; }

        [Required]
        [Range(0,100)]
        public int RoleID { get; set; }

        [Required]
        [Range(0, 100)]
        public int PlatoonID { get; set; }

        [Required]
        public DateTime EntryDate { get; set; }

         private DateTime _ExitDate; 
        public DateTime ExitDate { get { return _ExitDate; } }

        [Required]
        public bool IsActive { get; set; }
    
    
  }
}
