﻿using System;
using System.Collections.Generic;

namespace EmployeeManagementService.Models
{
    public interface IEmployeeRepository
    {
        List<Employee> GetAllEmployees();
        Employee GetEmployee(int id);

        Employee AddEmployee(int id, string name, int roleId, int platoonId, DateTime entrydate, DateTime exitdate, bool isactive);

        void DeleteEmployee(Employee employee);

        Employee UpdateEmployee(int id, Employee employee);
        

    }
}
