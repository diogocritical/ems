﻿using EmployeeManagementService.Data;
using EmployeeManagementService.Models;
using System.Collections.Generic;

namespace EmployeeManagementService.Repository
{
    public class PlatoonRepository : IPlatoonRepository
    {
        public List<Platoon> GetAllPlatoons()
        {
            return PlatoonsData.platoonsdata.Platoons;
        }
        public Platoon AddPlatoon(int id, string name)
        {
            if (PlatoonsData.platoonsdata.Platoons.Find(x => x.ID == id) == null && PlatoonsData.platoonsdata.Platoons.Find(x => x.Name == name) == null)
            {
                Platoon p = new Platoon();
                p.ID= id;
                p.Name= name;
                PlatoonsData.platoonsdata.Platoons.Add(p);
                return p;
            }
            else
                return null;
        }

        public void DeletePlatoon(Platoon platoon)
        {
            PlatoonsData.platoonsdata.Platoons.Remove(platoon);    
        }

        public Platoon GetPlatoon(int id)
        {
            return PlatoonsData.platoonsdata.Platoons.Find(x => x.ID == id);
        }
    }
}
