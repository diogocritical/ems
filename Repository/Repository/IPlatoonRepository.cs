﻿using System.Collections.Generic;

namespace EmployeeManagementService.Models
{
    public interface IPlatoonRepository
    {
        List<Platoon> GetAllPlatoons();
        Platoon AddPlatoon(int id, string name);
        void DeletePlatoon(Platoon platoon);
        Platoon GetPlatoon(int id);
    }
}
