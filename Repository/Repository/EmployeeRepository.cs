﻿using EmployeeManagementService.Data;
using EmployeeManagementService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeManagementService.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public List<Employee> GetAllEmployees()
        {
            return EmployeesData.employeesdata.Employees; 
        }

        public Employee GetEmployee(int id)
        {
            return EmployeesData.employeesdata.Employees.Find(x => x.ID == id); 
        }

        public Employee AddEmployee(int id, string name, int role, int platoon, DateTime entrydate, DateTime exitdate, bool isactive)
        {
            Employee e = new Employee();
            e.ID = id;
            e.Name = name;
            e.RoleID = role;
            e.PlatoonID = platoon;
            e.EntryDate = entrydate;
            e.IsActive = isactive;
            EmployeesData.employeesdata.Employees.Add(e);
            return e;
        }

        public void DeleteEmployee(Employee employee)
        {
            EmployeesData.employeesdata.Employees.Remove(employee);
        }

        public Employee UpdateEmployee(int id,Employee employee)
        {
            var e = EmployeesData.employeesdata.Employees.Find(x => x.ID == id);
            e.Name = employee.Name;
            e.RoleID = employee.RoleID;
            e.PlatoonID = employee.PlatoonID;
            e.EntryDate = employee.EntryDate;
            e.IsActive = employee.IsActive;
            return e;
        }
    }
}
