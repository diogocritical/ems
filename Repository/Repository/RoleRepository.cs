﻿using EmployeeManagementService.Data;
using EmployeeManagementService.Models;
using System.Collections.Generic;

namespace EmployeeManagementService.Repository
{
        public class RoleRepository : IRoleRepository
        {
            public List<Role> GetAllRoles()
            {
                return RolesData.rolesdata.Roles;
            }
            public Role AddRole(int id, string name)
            {
                if (RolesData.rolesdata.Roles.Find(x => x.ID == id) == null)
                {
                    Role p = new Role();
                    p.ID=id;
                    p.Name=name;
                    RolesData.rolesdata.Roles.Add(p);
                    return p;
                }
                else
                    return null;
            }

            public void DeleteRole(Role role)
            {
                RolesData.rolesdata.Roles.Remove(role);
            }

            public Role GetRole(int id)
            {
                return RolesData.rolesdata.Roles.Find(x => x.ID == id);
            }

    }
}
