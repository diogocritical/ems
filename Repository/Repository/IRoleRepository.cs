﻿using System.Collections.Generic;

namespace EmployeeManagementService.Models
{
    public interface IRoleRepository
    {
        List<Role> GetAllRoles();
        Role AddRole(int id, string name);
        void DeleteRole(Role role);
        Role GetRole(int id);
    }
}
