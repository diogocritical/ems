﻿using EmployeeManagementService.Models;
using System;
using System.Collections.Generic;

namespace EmployeeManagementService.Data
{
    public class EmployeesData
    {
        public static EmployeesData employeesdata { get; } = new EmployeesData();
        public List<Employee> Employees { get; set; }

        public EmployeesData ()
        {
            Employees = new List<Employee>()
            {
                new Employee{ID=1, PlatoonID=1, IsActive=true, RoleID=1, Name="Diogo", EntryDate=Convert.ToDateTime("07/04/2022")},
                new Employee{ID=2, PlatoonID=2, IsActive=true,RoleID=2, Name="Jose", EntryDate=Convert.ToDateTime("07/04/2022")},
                new Employee{ID=3, PlatoonID=3, IsActive=true,RoleID=1, Name="Marcus", EntryDate=Convert.ToDateTime("06/03/2022")}
            };
        }
    }
}
