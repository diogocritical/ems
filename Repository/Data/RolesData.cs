﻿using EmployeeManagementService.Models;
using System.Collections.Generic;

namespace EmployeeManagementService.Data
{
    public class RolesData
    {
        public static RolesData rolesdata = new RolesData();
        public List<Role> Roles { get; set; }

        public RolesData()
        {
            Roles = new List<Role>()
            {
                new Role{ID = 1, Name ="BackendDeveloper", Members=1},
                new Role{ID = 2, Name ="FrontendDeveloper", Members=2},
                new Role{ID = 3, Name ="Tester", Members=10}
            };
        }
    }
}
