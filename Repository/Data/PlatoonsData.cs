﻿using EmployeeManagementService.Models;
using System.Collections.Generic;

namespace EmployeeManagementService.Data
{
    public class PlatoonsData
    {
        public static PlatoonsData platoonsdata= new PlatoonsData();
        public List<Platoon> Platoons { get; set; }

        public PlatoonsData()
        {
            Platoons = new List<Platoon>()
            {
                new Platoon{ID =1, Name="Spartans"},
                new Platoon{ID =2, Name="Mandalorians"},
                new Platoon{ID=3, Name="Origami"}
            };
        }
    }
}
