﻿using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using System.Collections.Generic;

namespace EmployeeManagementService.Services
{
    public interface IPlatoonService
    {
        List<PlatoonDto> GetAllPlatoons();
        Platoon AddPlatoon(int id, string name);
        void DeletePlatoon(int id);
        Platoon GetPlatoon(int id);
    }
}
