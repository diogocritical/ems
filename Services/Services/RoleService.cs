﻿using AutoMapper;
using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using System;
using System.Collections.Generic;

namespace EmployeeManagementService.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public RoleService(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public List<RoleDto> GetAllRoles()
        {
            return _mapper.Map<List<RoleDto>>(_roleRepository.GetAllRoles());
        }

        public Role AddRole(int id, string name)
        {
            var rep = _roleRepository.GetAllRoles();
            if (rep.Find(x => x.ID == id) == null)
            {
                Role p = new Role();
                p.ID = id;
                p.Name = name;
                rep.Add(p);
                _mapper.Map<RoleDto>(p);
                return p;
            }
            else
            {
                throw new Exception("Role already exists");
            }
        }

        public void DeleteRole(int id)
        {
            var rep = _roleRepository.GetAllRoles().Find(x => x.ID == id);
            if (rep == null)
            {
                throw new Exception("Role not found");
            }

            _roleRepository.DeleteRole(rep);

        }

        public Role GetRole(int id)
        {
            var p = _roleRepository.GetAllRoles().Find(x => x.ID == id);
            return p;
        }
    }
}
