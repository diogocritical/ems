﻿namespace EmployeeManagementService.Dtos
{
    public class RoleDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Members { get; set; }

    }
}
