﻿

namespace EmployeeManagementService.Dtos
{
    public class EmployeeDto
    {
        
        public int ID { get; set; }
        public string Name { get; set; }
        public int RoleID { get; set; }
        public int PlatoonID { get; set; }


    }
}
