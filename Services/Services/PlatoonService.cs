﻿using AutoMapper;
using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using EmployeeManagementService.Repository;
using System;
using System.Collections.Generic;

namespace EmployeeManagementService.Services
{
    public class PlatoonService : IPlatoonService
    {
        private readonly IPlatoonRepository _platoonRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;

        public PlatoonService(IPlatoonRepository platoonRepository, IMapper mapper, IEmployeeRepository employeeRepository)
        {
            _platoonRepository = platoonRepository;
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public List<PlatoonDto> GetAllPlatoons()
        {
            return _mapper.Map<List<PlatoonDto>>(_platoonRepository.GetAllPlatoons());
        }

        public Platoon AddPlatoon(int id, string name)
        {
            var rep= _platoonRepository.GetAllPlatoons();
            if (rep.Find(x => x.ID == id) == null && rep.Find(x => x.Name == name) == null)
            {
                Platoon p = new Platoon();
                p.ID = id;
                p.Name = name;
                rep.Add(p);
                _mapper.Map<PlatoonDto>(p);
                return p;
            }
            else
            {
                throw new Exception("Platoon already exists");
            }
                
        }

        public void DeletePlatoon(int id)
        {
            var rep= _platoonRepository.GetAllPlatoons().Find(x => x.ID == id);
            if (rep == null)
            {
                throw new Exception("Platoon not found");
                
            }

            var employees= _employeeRepository.GetAllEmployees().Find(x=>x.PlatoonID == id);
            if(employees!=null)
            {
                throw new Exception("There are still employees in this platoon");
            }
            _platoonRepository.DeletePlatoon(rep);

        }

        public Platoon GetPlatoon(int id)
        {
            var p= _platoonRepository.GetAllPlatoons().Find(x=>x.ID==id);
            return p;
        }


    }
}
