﻿using AutoMapper;
using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;

namespace EmployeeManagementService.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPlatoonRepository _platoonRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public EmployeeService(IEmployeeRepository employeeRepository, IPlatoonRepository platoonRepository, IRoleRepository roleRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _platoonRepository = platoonRepository;
            _roleRepository = roleRepository;
            _mapper = mapper;
        }
        public List<EmployeeDto> GetAllEmployees()
        {
            return _mapper.Map<List<EmployeeDto>>(_employeeRepository.GetAllEmployees());
        }

        public Employee GetEmployee(int id)
        {
            var e = _employeeRepository.GetAllEmployees().Find(x => x.ID == id);
            if (e != null)
            {
                _mapper.Map<EmployeeDto>(e);
                return e;
            }
            else
            {
                throw new Exception("Employee not found");
            }
                
        }

        public Employee AddEmployee(int id, string name, int roleId, int platoonId, DateTime entrydate, DateTime exitdate, bool isactive)
        {
            var e = _employeeRepository.GetAllEmployees();
            var r = _roleRepository.GetAllRoles();
            var p = _platoonRepository.GetAllPlatoons();
            var eId = e.Find(x => x.ID == id);
            var rId = r.Find(x => x.ID == roleId);
            var pId = p.Find(x => x.ID == platoonId);
            if (eId == null && rId != null && pId != null)
            {
                var employee = _employeeRepository.AddEmployee(id, name, roleId, platoonId, entrydate, exitdate, isactive);
                rId.Members++;
                pId.Members++;
                var edto = _mapper.Map<EmployeeDto>(employee);
                return employee;
            }
            else
            {
                throw new Exception("Employee already exists or invalid assignment to role/platoon");
            }
        }

        public void DeleteEmployee(Employee employee)
        {
            var e= _employeeRepository.GetEmployee(employee.ID);
            if(e != null)
            {
                _employeeRepository.DeleteEmployee(employee);
            }
            else
            {
                throw new Exception("Employee not found");
            }
        }

        public Employee UpdateEmployee(int id, Employee employee)
        {
            var e = _employeeRepository.GetEmployee(id);
            if(e!=null)
            {
                e.Name = employee.Name;
                e.RoleID = employee.RoleID;
                e.PlatoonID = employee.PlatoonID;
                e.EntryDate = employee.EntryDate;
                e.IsActive = employee.IsActive;
                _mapper.Map<EmployeeDto>(e);
                return e;
            }
            else
            {
                throw new Exception("Employee not found");
            }
            
            
        }

        public Employee ChangeEmployee(int id, JsonPatchDocument<Employee> patchDoc)
        {
            var e = _employeeRepository.GetEmployee(id);
            if (e != null)
            {
                patchDoc.ApplyTo(e);
                
                var role= _roleRepository.GetRole(e.RoleID);
                var platoon = _platoonRepository.GetPlatoon(e.PlatoonID);
                role.Members--; 
                platoon.Members--;
                _mapper.Map<EmployeeDto>(e);

                return e;
            }
            else
            {
                throw new Exception("Employee not found");
            }
        }

        //talvez criar update parcial e adicionar as exceptions em todos os services
    }
}

