﻿using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using System.Collections.Generic;

namespace EmployeeManagementService.Services
{
    public interface IRoleService
    {
        List<RoleDto> GetAllRoles();
        Role AddRole(int id, string name);
        void DeleteRole(int id);
        Role GetRole(int id);
    }
}
