﻿using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;

namespace EmployeeManagementService.Services
{
    public interface IEmployeeService 
    {
        List<EmployeeDto> GetAllEmployees();
        Employee GetEmployee(int id);

        Employee AddEmployee(int id, string name, int roleId, int platoonId, DateTime entrydate, DateTime exitdate, bool isactive);

        void DeleteEmployee(Employee employee);

        Employee UpdateEmployee(int id, Employee employee);

        Employee ChangeEmployee(int id, JsonPatchDocument<Employee> patchDoc);
    }
}
