﻿using Microsoft.AspNetCore.Mvc;
using EmployeeManagementService.Models;
using System.Collections.Generic;
using EmployeeManagementService.Repository;
using AutoMapper;
using EmployeeManagementService.Dtos;
using Microsoft.AspNetCore.Http;
using EmployeeManagementService.Services;
using System;

namespace EmployeeManagementService.Controllers
{
    [Route("api/roles")]
    [ApiController]
    [Produces("application/json")]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        // List all roles
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<RoleDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public IActionResult Get()
        {
            var role = _roleService.GetAllRoles();
            if (role!=null)
            {
                return Ok(role);
            }
            else
                return Problem("The server encountered an internal error or misconfiguration and was unable to complete your request");
        }

        // Add a new role
        [HttpPost]
        [ProducesResponseType(typeof(RoleDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post([FromBody] Role role)
        {
            try
            {
                var new_role = _roleService.AddRole(role.ID, role.Name);
                return Ok(new_role);
            }

            catch(Exception ex)
            {
                var problemDetails = new ProblemDetails
                {
                    Status = StatusCodes.Status400BadRequest,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                    Title = "Bad Request",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };
                return BadRequest(problemDetails);
            }
                
        }

       
        // Delete a role
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            try
            {
                _roleService.DeleteRole(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                var probs = new ProblemDetails
                {
                    Status = StatusCodes.Status404NotFound,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                    Title = "Not Found",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };

                return NotFound(probs);
            }
            
        }
    }
}
