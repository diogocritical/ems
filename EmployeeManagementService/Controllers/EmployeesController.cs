﻿using EmployeeManagementService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagementService.Repository;
using AutoMapper;
using EmployeeManagementService.Dtos;
using Microsoft.AspNetCore.JsonPatch;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using EmployeeManagementService.Services;


namespace EmployeeManagementService.Controllers
{
    [Route("api/employees")]
    [ApiController]
    [Produces("application/json")]

    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        // List all employees
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<EmployeeDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public IActionResult Get()
        {
            var employees= _employeeService.GetAllEmployees();
            if (employees != null)
            {
                return Ok(employees);
            }
            else
                return Problem("The server encountered an internal error or misconfiguration and was unable to complete your request");
        }

        // Have the details of an employee
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(EmployeeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            try
            {
                var employee= _employeeService.GetEmployee(id);
                return Ok(employee);
            }
            catch (Exception ex)
            {
                var probs = new ProblemDetails
                {
                    Status = StatusCodes.Status404NotFound,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                    Title = "Not Found",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };

                return NotFound(probs);
            }
            
        }

        // Create a new employee
        [HttpPost]
        [ProducesResponseType(typeof(EmployeeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post([FromBody] Employee employee)
        {
            try
            {
                var new_e = _employeeService.AddEmployee(employee.ID, employee.Name, employee.RoleID, employee.PlatoonID, employee.EntryDate, employee.ExitDate, employee.IsActive);
                return Ok(new_e);
            }
            catch(Exception ex)
            {
                var problemDetails = new ProblemDetails
                {
                    Status = StatusCodes.Status400BadRequest,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                    Title = "Bad Request",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };
                return BadRequest(problemDetails);
            }           
        }

        //Full employee update
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(EmployeeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Put(int id, [FromBody] Employee employee)
        {
            try
            {
                var newe = _employeeService.UpdateEmployee(id, employee);
                return Ok(newe);
            }
            catch (Exception ex)
            {
                var probs = new ProblemDetails
                {
                    Status = StatusCodes.Status404NotFound,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                    Title = "Not Found",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };

                return NotFound(probs);
            }

        }

        // Partial employee update
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(EmployeeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Patch(int id, [FromBody] JsonPatchDocument<Employee> patchDoc)
        {
            try
            {
                var newe = _employeeService.ChangeEmployee(id, patchDoc);
                return Ok(newe);
            }
            catch(Exception ex)
            {
                var probs = new ProblemDetails
                {
                    Status = StatusCodes.Status404NotFound,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                    Title = "Not Found",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };

                return NotFound(probs);
            }
        }

        // Delete an employee
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            try
            {
                var employee = _employeeService.GetEmployee(id);
                _employeeService.DeleteEmployee(employee);
                return NoContent();
            }

            catch (Exception ex)
            {
                var probs = new ProblemDetails
                {
                    Status = StatusCodes.Status404NotFound,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                    Title = "Not Found",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };

                return NotFound(probs);
            }

        }

    }
}
