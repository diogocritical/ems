﻿using AutoMapper;
using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;
using EmployeeManagementService.Repository;
using EmployeeManagementService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;


namespace EmployeeManagementService.Controllers
{
    [Route("api/platoons")]
    [ApiController]
    [Produces("application/json")]
    public class PlatoonsController : ControllerBase
    {
        private readonly IPlatoonService _platoonService;

        public PlatoonsController(IPlatoonService platoonService)
        {
            _platoonService = platoonService;
        }

        // List all platoons
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PlatoonDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public IActionResult Get()
        {
            if (_platoonService.GetAllPlatoons() != null)
                return Ok(_platoonService.GetAllPlatoons());
            else
                return Problem("The server encountered an internal error or misconfiguration and was unable to complete your request");
        }

        // Add a new platoon
        [HttpPost]
        [ProducesResponseType(typeof(PlatoonDto),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        
        public IActionResult Post([FromBody] Platoon platoon)
        {
            try
            {
                var p = _platoonService.AddPlatoon(platoon.ID, platoon.Name);
                return Ok(platoon);
            }

            catch(Exception ex)
            {
                var problemDetails = new ProblemDetails
                {
                    Status = StatusCodes.Status400BadRequest,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                    Title = "Bad Request",
                    Detail = ex.Message,
                    Instance = HttpContext.Request.Path
                };
                return BadRequest(problemDetails);
            }
        }

        // Delete a platoon
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            try
            {
                _platoonService.DeletePlatoon(id);
                return NoContent();
            }

            catch (Exception ex)
            {
                if(ex.Message=="Platoon not found")
                {
                    var probs = new ProblemDetails
                    {
                        Status = StatusCodes.Status404NotFound,
                        Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                        Title = "Not Found",
                        Detail = ex.Message,
                        Instance = HttpContext.Request.Path
                    };

                    return NotFound(probs);
                }
                else
                {
                    var probs = new ProblemDetails
                    {
                        Status = StatusCodes.Status400BadRequest,
                        Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                        Title = "Bad Request",
                        Detail = ex.Message,
                        Instance = HttpContext.Request.Path
                    };

                    return BadRequest(probs);
                }

                    
            }
        }
    }
}
