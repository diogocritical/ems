﻿using AutoMapper;
using EmployeeManagementService.Dtos;
using EmployeeManagementService.Models;

namespace EmployeeManagementService.Profiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Role, RoleDto>();
            CreateMap<Platoon, PlatoonDto>();
            CreateMap<Employee, EmployeeDto>();
                
        }
    }
}
